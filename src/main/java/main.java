import Models.*;
import ObjectService.*;
import com.google.gson.Gson;
import io.javalin.Javalin;


public class main {
    public static void main(String[] args) {
//        BasicConfigurator.configure();
       // enableCORS("*","*","*");

            Javalin app = Javalin.create();
            app.enableCorsForAllOrigins();

            app.start(7000);
            app.get("/", ctx -> ctx.result("Hello World"));



        AanbiedingServices aanbiedingServices = new AanbiedingServices();
        app.post("/aanbiedingen", (ctx) -> {

            Aanbieding aanbieding = new Gson().fromJson(ctx.body(), Aanbieding.class);
            aanbiedingServices.insertObject(aanbieding);
            ctx.result( "succes");
        });
        app.get("/aanbiedingen", (ctx) -> {

            ctx.result( new Gson().toJson(aanbiedingServices.getAllObjects()));
        });
        app.get("/aanbiedingen/:id", (ctx) -> {

            ctx.result( new Gson().toJson(aanbiedingServices.getObject(Integer.parseInt(ctx.pathParam("id")))));
        });
        app.put("/aanbiedingen/id", (ctx) -> {

            Aanbieding toEdit = new Gson().fromJson(ctx.body(), Aanbieding.class);
            Aanbieding editedAanbiedingt = (Aanbieding)aanbiedingServices.updateObject(toEdit);

            if (editedAanbiedingt != null) {
                ctx.result( new Gson().toJson(editedAanbiedingt));
            } else {
                ctx.result( new Gson().toJson("User not found or error in edit"));
            }
        });
        app.delete("/aanbiedingen/id", (ctx) -> {

            aanbiedingServices.deleteObject(ctx.pathParam("id"));
            ctx.result( new Gson().toJson("user deleted"));
        });

        AccountServices accountServices = new AccountServices();
        app.post("/accounts", (ctx) -> {

            Account account = new Gson().fromJson(ctx.body(), Account.class);
            accountServices.insertObject(account);
            ctx.result( "succes");
        });
        app.get("/accounts", (ctx) -> {

            ctx.result( new Gson().toJson(accountServices.getAllObjects()));
        });
        app.get("/accounts/id", (ctx) -> {

            ctx.result( new Gson().toJson(accountServices.getObject(Integer.parseInt(ctx.pathParam("id")))));
        });
        app.put("/accounts/id", (ctx) -> {

            Account toEdit = new Gson().fromJson(ctx.body(), Account.class);
            Account editedAccount = (Account)accountServices.updateObject(toEdit);

            if (editedAccount != null) {
                ctx.result( new Gson().toJson(editedAccount));
            } else {
                ctx.result( new Gson().toJson("User not found or error in edit"));
            }
        });
        app.delete("/accounts/id", (ctx) -> {

            accountServices.deleteObject(ctx.pathParam("id"));
            ctx.result( new Gson().toJson("user deleted"));
        });


        AdressServices adressServices = new AdressServices();
        app.post("/adresses", (ctx) -> {

            Adress adress = new Gson().fromJson(ctx.body(), Adress.class);
            adressServices.insertObject(adress);
            ctx.result( "succes");
        });
        app.get("/adresses", (ctx) -> {

            ctx.result( new Gson().toJson(adressServices.getAllObjects()));
        });
        app.get("/adresses/:id", (ctx) -> {

            ctx.result( new Gson().toJson(adressServices.getObject(Integer.parseInt(ctx.pathParam("id")))));
        });
        app.put("/adresses/id", (ctx) -> {

            Adress toEdit = new Gson().fromJson(ctx.body(), Adress.class);
            Adress editedAdress = (Adress)adressServices.updateObject(toEdit);

            if (editedAdress != null) {
                ctx.result( new Gson().toJson(editedAdress));
            } else {
                ctx.result( new Gson().toJson("User not found or error in edit"));
            }
        });
        app.delete("/adresses/id", ctx -> {

            adressServices.deleteObject(ctx.pathParam("id"));
            ctx.result( new Gson().toJson("user deleted"));
        });


        BestellingServices bestellingServices = new BestellingServices();
        app.post("/bestellingen", ctx -> {
            String c =(ctx.body());
            Object a =  new Gson().fromJson(ctx.body(),Bestelling.class);
            Bestelling bestelling = new Gson().fromJson(ctx.body(),Bestelling.class);

            bestellingServices.insertObject(bestelling);
            ctx.result(" ok " + bestelling.toString());
        });

        app.get("/bestellingen", (ctx) -> {
            ctx.result(new Gson().toJson(bestellingServices.getAllObjects()));
        });
        app.get("/bestellingen/:id", (ctx) -> {

            ctx.result(new Gson().toJson(bestellingServices.getObject(Integer.parseInt(ctx.pathParam("id")))));
        });
        app.put("/bestellingen/id", ( ctx) -> {

            Bestelling toEdit = new Gson().fromJson(ctx.body(), Bestelling.class);
            Bestelling editedBestelling = (Bestelling)bestellingServices.updateObject(toEdit);

            if (editedBestelling != null) {
                ctx.result( new Gson().toJson(editedBestelling));
            } else {
                ctx.result( new Gson().toJson("User not found or error in edit"));
            }
        });
        app.delete("/bestellingen/id", (ctx) -> {

            bestellingServices.deleteObject(ctx.pathParam("id"));
            ctx.result( new Gson().toJson("user deleted"));
        });

        BestellingsregelServices bestellingsregelServices = new BestellingsregelServices();
        app.post("/bestellingsregels", (ctx) -> {

            Bestellingsregel bestellingsregel = new Gson().fromJson(ctx.body(), Bestellingsregel.class);
            bestellingsregelServices.insertObject(bestellingsregel);
            ctx.result( "succes");
        });
        app.get("/bestellingsregels", (ctx) -> {
            ctx.result( new Gson().toJson(bestellingsregelServices.getAllObjects()));
        });
        app.get("/bestellingsregels/:id", (ctx) -> {
            ctx.result( new Gson().toJson(bestellingsregelServices.getObject(Integer.parseInt(ctx.pathParam("id")))));
        });
        app.put("/bestellingsregels/id", (ctx) -> {
            Bestellingsregel toEdit = new Gson().fromJson(ctx.body(), Bestellingsregel.class);
            Bestellingsregel editedProduct = (Bestellingsregel)bestellingsregelServices.updateObject(toEdit);

            if (editedProduct != null) {
                ctx.result( new Gson().toJson(editedProduct));
            } else {
                ctx.result( new Gson().toJson("User not found or error in edit"));
            }
        });
        app.delete("/bestellingsregels/id", (ctx) -> {

            bestellingsregelServices.deleteObject(ctx.pathParam("id"));
            ctx.result( new Gson().toJson("user deleted"));
        });

        CatagoryServices catagoryServices = new CatagoryServices();
        app.post("/catagory", (ctx) -> {

            Catagory catagory = new Gson().fromJson(ctx.body(), Catagory.class);
            catagoryServices.insertObject(catagory);
            ctx.result( "succes");
        });
        app.get("/catagory", (ctx) -> {

            ctx.result( new Gson().toJson(catagoryServices.getAllObjects()));
        });
        app.get("/catagory/:id", (ctx) -> {

            ctx.result( new Gson().toJson(catagoryServices.getObject(Integer.parseInt(ctx.pathParam("id")))));
        });
        app.put("/catagory/id", (ctx) -> {

            Catagory toEdit = new Gson().fromJson(ctx.body(), Catagory.class);
            Catagory editedcatagory = (Catagory)catagoryServices.updateObject(toEdit);

            if (editedcatagory != null) {
                ctx.result( new Gson().toJson(editedcatagory));
            } else {
                ctx.result( new Gson().toJson("User not found or error in edit"));
            }
        });
        app.delete("/catagory/id", (ctx) -> {

            catagoryServices.deleteObject(ctx.pathParam("id"));
            ctx.result( new Gson().toJson("user deleted"));
        });

        KlantServices klantServices = new KlantServices();
        app.post("/klanten", (ctx) -> {

            Klant klant = new Gson().fromJson(ctx.body(), Klant.class);
            klantServices.insertObject(klant);
            ctx.result( "succes");
        });
        app.get("/klanten", (ctx) -> {

            ctx.result( new Gson().toJson(klantServices.getAllObjects()));
        });
        app.get("/klanten/:id", (ctx) -> {

            ctx.result( new Gson().toJson(klantServices.getObject(Integer.parseInt(ctx.pathParam("id")))));
        });
        app.put("/klanten/id", (ctx) -> {

            Klant toEdit = new Gson().fromJson(ctx.body(), Klant.class);
            Klant editedKlant = (Klant)klantServices.updateObject(toEdit);

            if (editedKlant != null) {
                ctx.result( new Gson().toJson(editedKlant));
            } else {
                ctx.result( new Gson().toJson("User not found or error in edit"));
            }
        });
        app.delete("/klanten/id", (ctx) -> {

            klantServices.deleteObject(ctx.pathParam("id"));
            ctx.result( new Gson().toJson("user deleted"));
        });



        ProductServices productServices = new ProductServices();
        app.post("/products", (ctx) -> {

            Product product = new Gson().fromJson(ctx.body(), Product.class);
            productServices.insertObject(product);
            ctx.result( "succes");
        });
        app.get("/products", (ctx) -> {
            ctx.result( new Gson().toJson(productServices.getAllObjects()));
        });
        app.get("/products/:id", (ctx) -> {

            ctx.result( new Gson().toJson(productServices.getObject(Integer.parseInt(ctx.pathParam("id")))));
        });
        app.put("/products/id", (ctx) -> {

            Product toEdit = new Gson().fromJson(ctx.body(), Product.class);
            Product editedProduct = (Product)productServices.updateObject(toEdit);

            if (editedProduct != null) {
                ctx.result( new Gson().toJson(editedProduct));
            } else {
                ctx.result( new Gson().toJson("User not found or error in edit"));
            }
        });
        app.delete("/products/id", (ctx) -> {

            productServices.deleteObject(ctx.pathParam("id"));
            ctx.result( new Gson().toJson("user deleted"));
        });
        app.get("/hello/:name", (ctx) -> {
             String a = ctx.pathParam(":name");
            ctx.result( "Hello: " + ctx.pathParam(":name"));
        });

// Enables CORS on ctxs. This method is an initialization method and should be called once.
        }
}