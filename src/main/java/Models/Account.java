package Models;
import sun.util.calendar.CalendarDate;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Account {
    private int Id;
    private LocalDate OpenDate;
    private boolean IsActief;


    private Adress factuuradres;

    private List<Bestelling> bestellingen = new LinkedList<>();


    public Account(int id, boolean isActief, LocalDate openDate) {
        Id = id;
        IsActief = isActief;
        this.OpenDate = openDate;
    }

    public Adress getFactuuradres() {
        return factuuradres;
    }

    public void setFactuuradres(Adress factuuradres) {
        this.factuuradres = factuuradres;
    }

    public List<Bestelling> getBestellingen() {
        return bestellingen;
    }

    public void setBestellingen(List<Bestelling> bestellingen) {
        this.bestellingen = bestellingen;
    }
    public void addBestellingen(Bestelling bestelling) {
        this.bestellingen.add(bestelling);
    }
}

