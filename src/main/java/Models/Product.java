package Models;
import java.util.LinkedList;
import java.util.List;

public class Product {
    private int Id;
    private String Naam;
    private int Prijs;
    private List<Aanbieding> aanbiedingen = new LinkedList<>();


    public Product(String naam, int prijs) {
        Naam = naam;
        Prijs = prijs;
    }

    public void setId(int id) {
        Id = id;
    }

    public List<Aanbieding> getAanbiedingen() {
        return aanbiedingen;
    }

    public void setAanbiedingen(List<Aanbieding> aanbiedingen) {
        this.aanbiedingen = aanbiedingen;
    }
    public void addAanbieding(Aanbieding aanbieding) {
        this.aanbiedingen.add(aanbieding);
    }
}
