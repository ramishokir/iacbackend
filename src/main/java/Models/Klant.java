package Models;

public class Klant {
    private int Id;
    private String Naam;
    private String Afbeelding;
    private Adress adress;
    private Account account;

    public Klant(String naam, String afbeelding, Adress woonAdres,Account account ) {
        Naam = naam;
        Afbeelding = afbeelding;
        adress = woonAdres;
        this.account = account;
    }
    public void setId(int id) {
        Id = id;
    }


}
