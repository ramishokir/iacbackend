package Models;

public class Adress {
    private int id;
    private String straat;
    private int StraatNummer;

    public Adress(int id, String straat, int straatNummer) {
        this.id = id;
        this.straat = straat;
        StraatNummer = straatNummer;
    }

    public int getStraatNummer() {
        return StraatNummer;
    }

    public void setStraatNummer(int straatNummer) {
        StraatNummer = straatNummer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStraat() {
        return straat;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }
}
