package Models;

import sun.util.calendar.CalendarDate;

import java.util.Calendar;

public class Aanbieding {
    private int Id;
    private String Naam;
    private Calendar VanDatum;
    private Calendar TotDatum;
    private Product product;
    private int verschil;

    public Aanbieding(int id,String naam, Calendar vanDatum, Calendar totDatum,int verschil) {
        Id = id;
        this.Naam = naam;
        VanDatum = vanDatum;
        TotDatum = totDatum;
        this.verschil =verschil;

    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
