package Models;

import java.util.LinkedList;
import java.util.List;

public class Catagory {
    private int Id;
    private String name;
    private List<Product> producten = new LinkedList<>();

    public Catagory(int id, String name) {
        Id = id;
        this.name = name;
    }

    public List<Product> getProducten() {
        return producten;
    }

    public void setProducten(List<Product> producten) {
        this.producten = producten;
    }
    public void addProducten(Product product) {
        this.producten.add(product);
    }

}
