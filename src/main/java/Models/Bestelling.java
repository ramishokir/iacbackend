package Models;
import java.util.List;
public class Bestelling {
    private int Id;
    private Adress adress;
    private List<Bestellingsregel> bestellingsRegels;

    public Bestelling(int id, Adress adress, List<Bestellingsregel> bestellingsRegels) {
        Id = id;
        this.adress = adress;
        this.bestellingsRegels = bestellingsRegels;
    }
}
