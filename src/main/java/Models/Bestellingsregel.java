package Models;
import java.util.LinkedList;
import java.util.List;
public class Bestellingsregel {
    private int Id;
    private int aantal;
    private int price;
    private Product product;

    public Bestellingsregel(int id, int aantal, int price ,Product product) {
        Id = id;
        this.aantal = aantal;
        this.price = price;
        this.product = product;
    }
}
