package Database;

import Models.Account;
import Models.Adress;
import Models.Klant;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class KlantConnection implements DatabaseInferface{
    public void insertObject(Object NewObject) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make insertObject statement
    }

    public Object updateObject(Object updatedObject) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        Adress adress = new Adress(1,"molenstraat",2);
        LocalDate localDate = LocalDate.now();
        Account account = new Account(1, true, localDate);
        Klant klant = new Klant("rami","test" ,adress,account);
        return klant;
    }


    public Object getObject(int objectId) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make getObject statement
        return null;
    }

    public void deleteObject(Object object) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make deleteObject statement
    }
    public List<Object> getAllObjects() {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make getAll statement
        List<Object> klanten = new LinkedList<Object>();
        Adress adress = new Adress(1,"molenstraat",2);


        LocalDate localDate = LocalDate.now();
        Account account = new Account(1, true, localDate);
        Account account1 = new Account(2, false, localDate );

        Klant klant = new Klant("rami","test" ,adress,account);
        Klant klant2 = new Klant("rami2","test" ,adress,account1);

        klanten.add(klant);
        klanten.add(klant2);
        return klanten;
    }
}
