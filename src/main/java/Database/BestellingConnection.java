package Database;

import Models.Adress;
import Models.Bestelling;
import Models.Bestellingsregel;
import Models.Product;

import java.util.LinkedList;
import java.util.List;

public class BestellingConnection implements DatabaseInferface{
    public void insertObject(Object NewObject) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make insertObject statement
    }


    public Object updateObject( Object updatedObject) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        return  updatedObject;
    }


    public Object getObject(int objectId) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make getObject statement

        List<Bestellingsregel> Bestellingsregels = new LinkedList<Bestellingsregel>();
        Product product2 = new Product("ijs", 2);
        Bestellingsregel bestellingsRegel1 = new Bestellingsregel(1,2,2,product2);
        Bestellingsregel bestellingsRegel2 = new Bestellingsregel(2,2,2,product2);
        Bestellingsregels.add(bestellingsRegel1);
        Bestellingsregels.add(bestellingsRegel2);

        Adress adress = new Adress(1,"molenstraat",2);

        Bestelling bestelling = new Bestelling(1,adress,Bestellingsregels);


        return bestelling;
    }

    public void deleteObject(Object object) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make deleteObject statement
    }
    public List<Object> getAllObjects() {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make getAll statement
        List<Object> bestellingen = new LinkedList<Object>();
        List<Bestellingsregel> Bestellingsregels = new LinkedList<Bestellingsregel>();
        List<Bestellingsregel> Bestellingsregels1 = new LinkedList<Bestellingsregel>();
        Product product2 = new Product("ijs", 2);
        Product product1 = new Product("brood", 2);
        Bestellingsregel bestellingsRegel1 = new Bestellingsregel(1,2,2,product2);
        Bestellingsregel bestellingsRegel2 = new Bestellingsregel(2,2,2,product1);
        Bestellingsregels.add(bestellingsRegel1);
        Bestellingsregels1.add(bestellingsRegel2);

        Adress adress = new Adress(1,"molenstraat",2);

        Bestelling bestelling = new Bestelling(1,adress,Bestellingsregels);
        Bestelling bestelling1 = new Bestelling(2,adress,Bestellingsregels1);
        bestellingen.add(bestelling);
        bestellingen.add(bestelling1);

        return bestellingen;

    }
}
