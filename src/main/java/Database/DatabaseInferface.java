package Database;

import java.util.List;

public interface DatabaseInferface {
    void insertObject(Object NewObject);
    Object updateObject(Object updatedObject);
    Object getObject(int objectId);
    void deleteObject(Object object);
    List<Object> getAllObjects();
}
