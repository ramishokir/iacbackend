package Database;

import Models.Aanbieding;
import Models.Product;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class AanbiedingConnection implements DatabaseInferface{
    public void insertObject(Object NewObject) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make insertObject statement
    }


    public Object updateObject( Object updatedObject) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        return  updatedObject;
    }


    public Object getObject(int objectId) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make getObject statement

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 2);
        Calendar cal2 = Calendar.getInstance();
        Product product = new Product("Brood", 1);

        Aanbieding aanbieding = new Aanbieding(1,"mega aanbieding", cal2, cal ,5);
        aanbieding.setProduct(product);

        return aanbieding;
    }

    public void deleteObject(Object object) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make deleteObject statement
    }
    public List<Object> getAllObjects() {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make getAll statement
        //make getAll statement
        List<Object> aanbiedingen = new LinkedList<Object>();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 2);
        Calendar cal2 = Calendar.getInstance();


        Product product = new Product("Brood", 1);
        Product product2 = new Product("ijs", 2);


        Aanbieding aanbieding = new Aanbieding(1,"mega aanbieding", cal2, cal ,5);
        aanbieding.setProduct(product);
        Aanbieding aanbieding1 = new Aanbieding(2, "hamstere aanbieding", cal2, cal ,10);
        aanbieding1.setProduct(product2);
        aanbiedingen.add(aanbieding);
        aanbiedingen.add(aanbieding1);

        return aanbiedingen;
    }
}
