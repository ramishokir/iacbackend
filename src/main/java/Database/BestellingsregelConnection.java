package Database;

import Models.Bestellingsregel;
import Models.Product;

import java.util.LinkedList;
import java.util.List;

public class BestellingsregelConnection implements DatabaseInferface{
    public void insertObject(Object NewObject) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make insertObject statement
    }

    public Object updateObject( Object updatedObject) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        return  updatedObject;
    }

    public Object getObject(int objectId) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make getObject statement
        Product product2 = new Product("ijs", 2);
        Bestellingsregel bestellingsRegel1 = new Bestellingsregel(1,2,2,product2);

        return bestellingsRegel1;
    }

    public void deleteObject(Object object) {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make deleteObject statement
    }
    public List<Object> getAllObjects() {
        String sqlConnection = databaseConnectionInfo.connectionPassword + databaseConnectionInfo.connectionString+databaseConnectionInfo.connectionUsername;
        //make getAll statement
        List<Object> Bestellingsregels = new LinkedList<Object>();
        Product product2 = new Product("ijs", 2);
        Bestellingsregel bestellingsRegel1 = new Bestellingsregel(1,2,2,product2);
        Bestellingsregel bestellingsRegel2 = new Bestellingsregel(2,2,2,product2);
        Bestellingsregels.add(bestellingsRegel1);
        Bestellingsregels.add(bestellingsRegel2);

        return Bestellingsregels;
    }
}
