package ObjectService;

import Database.AanbiedingConnection;

import java.util.List;

public class AanbiedingServices implements routes
{
    AanbiedingConnection connection = new AanbiedingConnection();
    public AanbiedingServices(){

    }
    @Override
    public void insertObject(Object NewObject) {
        try{

            connection.insertObject(NewObject);
        }
        catch(Exception e){
            e.toString();
        }
    }

    @Override
    public Object updateObject(Object updatedObject) {
        try{

            return connection.updateObject(updatedObject);
        }
        catch(Exception e){
            return  e.toString();
        }
    }

    @Override
    public Object getObject(int objectId) {
        try{

            return connection.getObject(objectId);
        }
        catch(Exception e){
            return e.toString();
        }
    }

    @Override
    public void deleteObject(Object object) {
        try{
            connection.deleteObject(object);
        }
        catch(Exception e){
            e.toString();
        }
    }

    @Override
    public List<Object> getAllObjects() {

        return connection.getAllObjects();
    }
}
