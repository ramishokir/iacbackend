package ObjectService;

import java.util.List;

public interface routes {
    void insertObject(Object NewObject);
    Object updateObject(Object updatedObject);
    Object getObject(int objectId);
    void deleteObject(Object object);
    List<Object> getAllObjects();
}
