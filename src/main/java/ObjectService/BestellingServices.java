package ObjectService;

import Database.BestellingConnection;

import java.util.List;

public class BestellingServices implements routes
{
    BestellingConnection connection = new BestellingConnection();
    public BestellingServices(){

    }
    @Override
    public void insertObject(Object NewObject) {
        try{

            connection.insertObject(NewObject);
        }
        catch(Exception e){
            e.toString();
        }
    }

    @Override
    public Object updateObject(Object updatedObject) {
        try{

            return connection.updateObject(updatedObject);
        }
        catch(Exception e){
            return  e.toString();
        }
    }

    @Override
    public Object getObject(int objectId) {
        try{

            return connection.getObject(objectId);
        }
        catch(Exception e){
            return e.toString();
        }
    }

    @Override
    public void deleteObject(Object object) {
        try{
            connection.deleteObject(object);
        }
        catch(Exception e){
            e.toString();
        }
    }

    @Override
    public List<Object> getAllObjects() {

        return connection.getAllObjects();
    }
}
